#pragma once

#ifdef ATOM_SHARED
#	ifdef ATOM_EXPORTS
#		define ATOM_API __declspec(dllexport)
#	else
#		define ATOM_API __declspec(dllimport)
#	endif
#else
#	define ATOM_API
#endif