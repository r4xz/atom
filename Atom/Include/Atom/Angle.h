#include "Atom/API.h"

namespace Atom {
	class ATOM_API Angle {
	public:
		// Create angle using deg unit.
		// @param angle - Degrees.
		// @return Angle instance.
		static Angle deg(float angle);

		// Create angle using rad unit.
		// @param angle - Radians.
		// @return Angle instance.
		static Angle rad(float angle);

		// Get angle degrees.
		// @param Angle value in degrees.
		float deg();

		// Get angle radians.
		// @param Angle value in radians.
		float rad();

		// Convert radians to degrees.
		// @param angle - Angle in radians.
		// @return Angle in degrees.
		static float toDeg(float angle);

		// Convert degrees to radians.
		// @param angle - Angle in degrees.
		// @return Angle in radians.
		static float toRad(float angle);

	private:
		// Initialize angle.
		// @param deg - Angle in degrees.
		Angle(float deg);

		float _rad;
		float _deg;
	};
}