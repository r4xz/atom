#pragma once
#include "Atom/Object.h"

namespace Atom {
	class ATOM_API Camera : public Object {
	public:
		enum Type {
			Orthographic,
			Perspective
		};

		Type type;

		Camera(Type type = Perspective);

	protected:
		// Do nothing (can be used in future to draw camera range in debug mode).
		void render(const Matrix4<>& projection, const Matrix4<>& view) const;
	};
}