#pragma once
#include "Atom/Vector4.h"
#include "Atom/Types.h"

namespace Atom {
	// RGBA (32 bit) color.
	typedef Vector4<uchar> Color;
}