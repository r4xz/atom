#pragma once
#include "Atom/API.h"
#include <Windows.h>
#include <gl/GL.h>
#include <gl/glext.h>

namespace Atom {
	extern PFNGLCREATESHADERPROC glCreateShader;
	extern PFNGLSHADERSOURCEPROC glShaderSource;
	extern PFNGLCOMPILESHADERPROC glCompileShader;
	extern PFNGLGETSHADERIVPROC glGetShaderiv;
	extern PFNGLGETSHADERINFOLOGPROC glGetShaderInfoLog;
	extern PFNGLATTACHSHADERPROC glAttachShader;
	extern PFNGLDETACHSHADERPROC glDetachShader;
	extern PFNGLDELETESHADERPROC glDeleteShader;

	extern PFNGLCREATEPROGRAMPROC glCreateProgram;
	extern PFNGLLINKPROGRAMPROC glLinkProgram;
	extern PFNGLVALIDATEPROGRAMPROC glValidateProgram;
	extern PFNGLGETPROGRAMIVPROC glGetProgramiv;
	extern PFNGLGETPROGRAMINFOLOGPROC glGetProgramInfoLog;
	extern PFNGLGETATTRIBLOCATIONPROC glGetAttribLocation;
	extern PFNGLGETUNIFORMLOCATIONPROC glGetUniformLocation;
	extern PFNGLUSEPROGRAMPROC glUseProgram;
	extern PFNGLDELETEPROGRAMPROC glDeleteProgram;

	extern PFNGLUNIFORM1FPROC glUniform1f;
	extern PFNGLUNIFORM1IPROC glUniform1i;
	extern PFNGLUNIFORM1UIPROC glUniform1ui;
	extern PFNGLUNIFORM2FPROC glUniform2f;
	extern PFNGLUNIFORM2IPROC glUniform2i;
	extern PFNGLUNIFORM2UIPROC glUniform2ui;
	extern PFNGLUNIFORM3FPROC glUniform3f;
	extern PFNGLUNIFORM3IPROC glUniform3i;
	extern PFNGLUNIFORM3UIPROC glUniform3ui;
	extern PFNGLUNIFORM4FPROC glUniform4f;
	extern PFNGLUNIFORM4IPROC glUniform4i;
	extern PFNGLUNIFORM4UIPROC glUniform4ui;
	extern PFNGLUNIFORMMATRIX2FVPROC glUniformMatrix2fv;
	extern PFNGLUNIFORMMATRIX3FVPROC glUniformMatrix3fv;
	extern PFNGLUNIFORMMATRIX4FVPROC glUniformMatrix4fv;

	extern PFNGLGENBUFFERSPROC glGenBuffers;
	extern PFNGLBINDBUFFERPROC glBindBuffer;
	extern PFNGLBUFFERDATAPROC glBufferData;
	extern PFNGLDELETEBUFFERSPROC glDeleteBuffers;

	extern PFNGLGENVERTEXARRAYSPROC glGenVertexArrays;
	extern PFNGLBINDVERTEXARRAYPROC glBindVertexArray;
	extern PFNGLENABLEVERTEXATTRIBARRAYPROC glEnableVertexAttribArray;
	extern PFNGLVERTEXATTRIBPOINTERPROC glVertexAttribPointer;
	extern PFNGLDISABLEVERTEXATTRIBARRAYPROC glDisableVertexAttribArray;
	extern PFNGLDRAWELEMENTSBASEVERTEXPROC glDrawElementsBaseVertex;
	extern PFNGLDELETEVERTEXARRAYSPROC glDeleteVertexArrays;

	// Initialize OpenGL Context.
	// @return True if procedures loaded successfully, false otherwise.
	ATOM_API bool initContext();
}