#pragma once
#include "Atom/FileStream.h"
#include "Atom/Types.h"

namespace Atom {
	class ATOM_API FileReader : public FileStream {
	public:
		FileReader();
		virtual ~FileReader();

		// Open file and set cursor at the beggining.
		// @param path - Path to the file.
		// @return True if successfully opened, false otherwise.
		virtual bool open(const char* path);

		// Read whole file (leave cursor at the end).
		// @return True if success, false otherwise.
		virtual bool readAll();

		// Get whole file (leave cursor at the end).
		// @return C-string pointer (cannot detect end-of-file).
		virtual const char* getAll();

		// Read specified block from file.
		// @param size - Block size in bytes.
		// @return True if success, false otherwise.
		virtual bool readBlock(long size);

		// Get specified block from file.
		// @param size - Block size in bytes.
		// @return C-string pointer (cannot detect end-of-file).
		virtual const char* getBlock(long size);

		// Read line from file.
		// @param breakAt - Max characters count per line.
		// @return True if success, false otherwise.
		virtual bool readLine(long breakAt = 1024);

		// Get line from file.
		// @param breakAt - Max characters count per line.
		// @return C-string pointer (cannot detect end-of-file).
		virtual const char* getLine(long breakAt = 1024);

		// Read char from file.
		// @return True if success, false otherwise.
		virtual bool readChar();

		// Get char from file.
		// @return Char (cannot detect end-of-file).
		virtual uchar getChar();

		// Get last readed data.
		// @return Buffer pointer.
		const char* buffer() const;

	protected:
		// Free buffer memory.
		virtual void dispose();

	private:
		char* _buffer;
		long _buflen;

		// Ensure buffer size to be greater or equal the specified size.
		// @param size - Minimum size of the buffer.
		void prepareBuffer(long size);
	};
}