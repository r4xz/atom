#pragma once
#include "Atom/API.h"
#include <cstdio>

namespace Atom {
	// Provide interface for common file operations
	// and ensure that the file will be closed.
	class ATOM_API FileStream {
	public:
		enum Cursor {
			Begin = SEEK_SET,
			Current = SEEK_CUR,
			End = SEEK_END
		};

		FileStream();
		virtual ~FileStream();

		// Open stream for specified file and set cursor at the beggining.
		// @param path - Path to the file.
		// @return True if successfully opened, false otherwise.
		virtual bool open(const char* path) = 0;

		// Check whether the file stream is ready.
		// @return True if open, false otherwise.
		virtual bool isOpen() const;

		// Get cursor position (offset for 'Begin' origin).
		// @return Non-negative number, or negative number if error occured.
		long cursor() const;

		// Set cursor (reading/writing) position.
		// @param origin - Cursor relative position.
		// @param offset - Offset of the cursor origin (i.e. 'End' origin and -1 offset is the last char).
		// @return True if cursor changed, false otherwise (unknown behaviour of the cursor position).
		bool setCursor(Cursor origin, long offset = 0);

		// Get file length.
		// @return File length, negative value if error occured.
		long length() const;

		// Close file stream.
		// Called automatically when releasing the object.
		virtual void close();

	protected:
		FILE* _stream;

	private:
		// Store buffered file size (in bytes).
		long _length;
	};
}