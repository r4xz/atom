#pragma once
#include "Atom/Size.h"
#include "Atom/Color.h"

namespace Atom {
	// Interface for RGBA bitmap.
	class ATOM_API Image {
	public:
		// Get/Set image pixel.
		// @param x, y - Pixel coordinate, upper left corner is point (0,0).
		// @return Bitmap pixel reference.
		virtual Color& operator()(uint x, uint y) = 0;

		// Get image size.
		// @return Zero values if the image does non exists, positive numbers otherwise.
		virtual Size<uint> size() const = 0;

		// Create blank bitmap of specified size.
		// @param size - Bitmap size in pixels.
		virtual void create(Size<uint> size) = 0;

		// Load image from file.
		// @param path - Path to the file.
		// @return True if successfully loaded, false otherwise.
		virtual bool load(const char* path) = 0;

		// Set uniform color of the entire image.
		// @param color - Bitmap color.
		virtual void repaint(Color color) = 0;

		// Destroy bitmap.
		virtual void dispose() = 0;

		virtual ~Image() {};
	};
}