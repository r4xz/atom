#pragma once
#include "Atom/Shader.h"
#include "Atom/Matrix4.h"

namespace Atom {
	class ATOM_API Material {
	public:
		// Create material.
		// @param shader - Shader used by material.
		Material(std::shared_ptr<const Shader> shader);

		// Get vertex attribute.
		// @return Shader attribute location.
		Shader::Location vertexAttrib() const;

		// Get normal attribute.
		// @return Shader attribute location.
		Shader::Location normalAttrib() const;

		// Get texture coordinate attribute.
		// @return Shader attribute location.
		Shader::Location texCoordAttrib() const;

		Shader::Location mvpUniform() const;

		// Active shader and update uniforms values.
		virtual void use(const Matrix4<>& projection, const Matrix4<>& view, const Matrix4<>& model) const = 0;

	protected:
		// Initialize material.
		// Cache required shader attributes and uniforms loctions.
		virtual void initialize() = 0;

		std::shared_ptr<const Shader> _shader;

		Shader::Location _vertexAttrib;
		Shader::Location _normalAttrib;
		Shader::Location _texCoordAttrib;

		Shader::Location _mvpUniform;
	};
}