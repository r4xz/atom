#pragma once
#include "Atom/Context.h"
#include "Atom/Vector3.h"
#include "Atom/Vector4.h"
#include "Atom/Types.h"
#include "Atom/Angle.h"
#include <cassert>

#define ARR_SIZE 4
#define ARR_LEN 16
#define INDEX(row, col) (col + ARR_SIZE*row)

namespace Atom {
	// Matrix 4x4 with row major order.
	template <class T = GLfloat>
	class ATOM_API Matrix4 {
	public:
		// Identity matrix.
		Matrix4() : Matrix4(1) {}

		// Custom matrix.
		// @param m00-33 Matrix cells in left-handed, row major order.
		Matrix4(
			T m00, T m01, T m02, T m03,
			T m10, T m11, T m12, T m13,
			T m20, T m21, T m22, T m23,
			T m30, T m31, T m32, T m33) {
			_m = new T[ARR_LEN];

			setRow(0, m00, m01, m02, m03);
			setRow(1, m10, m11, m12, m13);
			setRow(2, m20, m21, m22, m23);
			setRow(3, m30, m31, m32, m33);
		}

		// Copy constructor
		Matrix4(const Matrix4<T>& rht) {
			_m = new T[ARR_LEN];

			for (int i = 0; i < ARR_LEN; ++i) {
				_m[i] = rht._m[i];
			}
		}

		// Scalar matrix
		// (scalar multiple of the identity matrix).
		// @param value - Multiplication factor.
		Matrix4(T value) {
			_m = new T[ARR_LEN];

			for (uchar row = 0; row < ARR_SIZE; ++row) {
				for (uchar col = 0; col < ARR_SIZE; ++col) {
					set(row, col, row == col ? value : 0);
				}
			}
		}

		// Get matrix cell value at specified index.
		// @param index - Cell index in range 0-15.
		// @return Cell value.
		T operator()(uchar index) {
			assert(index < ARR_LEN);

			return _m[index];
		}

		// Get matrix cell value.
		// @param row - Row in range 0-3.
		// @param col - Column in range 0-3.
		// @return Cell value.
		T operator()(uchar row, uchar col) const {
			assert(row < ARR_SIZE && col < ARR_SIZE);

			return _m[INDEX(row, col)];
		}

		// Get matrix data pointer.
		// @param row - Row in range 0-3.
		// @param col - Column in range 0-3.
		// @return Cell pointer.
		const T* ptr() const {
			return _m;
		}

		// Set matrix cell value.
		// @param row - Row in range 0-3.
		// @param col - Column in range 0-3.
		// @param value - New cell value.
		void set(uchar row, uchar col, T value) {
			assert(row < ARR_SIZE && col < ARR_SIZE);

			_m[INDEX(row, col)] = value;
		}

		void operator*=(const Matrix4<T>& rht) {
			*this = *this * rht;
		}

		Matrix4<T>& operator=(const Matrix4<T>& rht) {
			for (int i = 0; i < ARR_LEN; ++i) {
				_m[i] = rht._m[i];
			}

			return *this;
		}

		Vector4<T> operator*(const Vector4<T>& rht) const {
			return Vector4<T>(
				_m[INDEX(0, 0)] * rht.x + _m[INDEX(0, 1)] * rht.y + _m[INDEX(0, 2)] * rht.z + _m[INDEX(0, 3)] * rht.w,
				_m[INDEX(1, 0)] * rht.x + _m[INDEX(1, 1)] * rht.y + _m[INDEX(1, 2)] * rht.z + _m[INDEX(1, 3)] * rht.w,
				_m[INDEX(2, 0)] * rht.x + _m[INDEX(2, 1)] * rht.y + _m[INDEX(2, 2)] * rht.z + _m[INDEX(2, 3)] * rht.w,
				_m[INDEX(3, 0)] * rht.x + _m[INDEX(3, 1)] * rht.y + _m[INDEX(3, 2)] * rht.z + _m[INDEX(3, 3)] * rht.w,
			);
		}

		Matrix4<T> operator*(const Matrix4<T>& rht) const {
			Matrix4<T> result(0);
			for (int i = 0; i < ARR_SIZE; ++i) {
				for (int j = 0; j < ARR_SIZE; ++j) {
					for (int k = 0; k < ARR_SIZE; ++k) {
						result._m[INDEX(i, j)] += _m[INDEX(i, k)] * rht._m[INDEX(k, j)];
					}
				}
			}

			return result;
		}

		// Orthographic projection.
		// @param left, right - Horizontal axis range.
		// @param top, bottom - Vertical axis range.
		// @param zNear, zFar - Min/Max Z rendered value.
		// @return Orthographic projection transformation.
		static Matrix4<T> orthographic(T left, T right, T bottom, T top, T zNear, T zFar) {
			assert(left < right && bottom < top);
			
			return Matrix4<T>(
				2/(right-left), 0, 0, -(right+left)/(right-left),
				0, 2/(top-bottom), 0, -(top+bottom)/(top-bottom),
				0, 0, -2/(zFar-zNear), -(zFar + zNear) / (zFar - zNear),
				0, 0, 0, 1
			);
		}

		// Perspective projection.
		// @param fov - Field of view (recommended 30-90 deg).
		// @param aspect - Screen ratio (width/height).
		// @param zNear, zFar - Min/Max Z rendered value.
		// @return Perspective projection transformation.
		static Matrix4<T> perspective(Angle fov, T aspect, T zNear, T zFar) {
			T tanHalfFov = tan(fov.rad() / 2);

			return Matrix4<T>(
				1 / (aspect*tanHalfFov), 0, 0, 0,
				0, 1 / tanHalfFov, 0, 0,
				0, 0, -(zFar + zNear) / (zFar - zNear), -(2 * zFar * zNear) / (zFar - zNear),
				0, 0, -1, 0
			);
		}

		void rotate(Angle angle, const Vector3<T>& axis) {
			// TODO: http://inside.mines.edu/fs_home/gmurray/ArbitraryAxisRotation/
			throw "not implemented";
		}

		void translate(const Vector3<T>& vec) {
			// TODO: http://www.codinglabs.net/article_world_view_projection_matrix.aspx
			throw "not implemented";
		}

		void scale(const Vector3<T>& vec) {
			// TODO: http://www.codinglabs.net/article_world_view_projection_matrix.aspx
			throw "not implemented";
		}

		// Look at the specified point.
		// @see: http://www.cs.virginia.edu/~gfx/Courses/1999/intro.fall99.html/lookat.html
		// @param eye - Camera position.
		// @param target - Point to look at.
		// @param up - Vector up (determine camera rotation).
		// @return Lookat transformation.
		static Matrix4<T> lookAt(const Vector3<T>& eye, const Vector3<T>& target, const Vector3<T>& up) {
			Vector3<T> f = (target - eye).normalized();
			Vector3<T> s = (f.cross(up)).normalized();
			Vector3<T> u = s.cross(f);

			return Matrix4<T>(
				s.x, s.y, s.z, -s.dot(eye),
				u.x, u.y, u.z, -u.dot(eye),
				-f.x, -f.y, -f.z, f.dot(eye),
				0, 0, 0, 1
			);
		}

		~Matrix4() {
			delete[] _m;
		}

	private:
		T* _m;

		// Fill row values.
		// @param row - Row index (0-3).
		// @param c0-c3 - Column values.
		inline void setRow(uchar row, T c0, T c1, T c2, T c3) {
			assert(row < ARR_SIZE);

			_m[INDEX(row, 0)] = c0;
			_m[INDEX(row, 1)] = c1;
			_m[INDEX(row, 2)] = c2;
			_m[INDEX(row, 3)] = c3;
		}
	};
}