#pragma once
#include "Atom/Object.h"
#include "Atom/Material.h"
#include "Atom/Types.h"
#include <vector>

namespace Atom {
	class ATOM_API Mesh : public Object {
	public:
		// Initialize mesh from buffers.
		// @param vao - OpenGL Vertex Array Objects.
		// @param buffers - Location of OpenGL buffers.
		// @param indices - Indices container.
		// @param material - Material using by renderer.
		Mesh(GLuint vao, const std::vector<GLuint>& buffers, const std::vector<uint>& indices, std::shared_ptr<const Material> material);

		// Draw object to currently used framebuffer.
		// @param projection - Projection matrix.
		// @param view - Camera transformation matrix.
		virtual void render(const Matrix4<>& projection, const Matrix4<>& view) const;

		// Free memory.
		~Mesh();

	protected:
		// Destroy buffers.
		virtual void dispose();

	private:
		GLuint _vao;
		std::vector<GLuint> _buffers;
		std::vector<uint> _indices;

		std::shared_ptr<const Material> _material;
	};
}