#pragma once
#include "Atom/Mesh.h"
#include "Atom/Vector3.h"
#include "Atom/Vector2.h"
#include "Atom/Types.h"
#include "Atom/Material.h"
#include <vector>

namespace Atom {
	class ATOM_API MeshBuilder {
	public:
		struct Indice {
			int vertex;
			int normal;
			int texCoord;
		};

		// Initialize builder.
		MeshBuilder();

		// Add vertex (indexed from 1).
		// @param vertex - Position vector.
		// @return Index of added vertex.
		int addVertex(Vector3<> vertex);

		// Get vertices count
		// (including default vector at index 0).
		// @return Positive number.
		int verticesCount() const;

		// Add normal (indexed from 1).
		// @param normal - Normal vector.
		// @return Index of added normal.
		int addNormal(Vector3<> normal);

		// Get normals count
		// (including default vector at index 0).
		// @return Positive number.
		int normalsCount() const;

		// Add texture coordinates (indexed from 1).
		// @param texCoord - Texture coordinates vector.
		// @return Index of added coords.
		int addTexCoord(Vector2<> texCoord);

		// Get texture coordinates count
		// (including default vector at index 0).
		// @return Positive number.
		int texCoordsCount() const;

		// Add indice.
		// Index less than zero means n-item counting from end (eq. -1 is the last element).
		// Use index 0 for zero vector (commonly used for normal/texCoord).
		// @param indice - Index of vertex, normal and texCoord.
		void addIndice(Indice indice);

		// Get indices count
		// (including default vector at index 0).
		// @return Positive number.
		uint indicesCount() const;

		// Add triangular face.
		// @param i1, i2, i3 - Triangle indices.
		void addFace(Indice i1, Indice i2, Indice i3);

		void clear();

		void clearIndices();

		// Get mesh object with data binded to buffers.
		// @param material - Material used to get buffers locations.
		// @return Pointer to mesh allocated in GPU.
		std::shared_ptr<Mesh> build(std::shared_ptr<const Material> material);

	private:
		std::vector<Vector3<>> _vertices;
		std::vector<Vector3<>> _normals;
		std::vector<Vector2<>> _texCoords;
		std::vector<Indice> _indices;

		// Prepare containers for OpenGL indexed draw
		// (ie. indice components are equal - vertex = normal = texCoord).
		// @return Simplified indices array.
		std::vector<uint> compact();
	};
}