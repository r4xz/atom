#pragma once
#include "Atom/Mesh.h"

namespace Atom {
	class ATOM_API Model : public Object {
	public:
		Model(std::vector<std::shared_ptr<Mesh>> meshes);

		// Draw object to currently used framebuffer.
		// @param projection - Projection matrix.
		// @param view - Camera transformation matrix.
		virtual void render(const Matrix4<>& projection, const Matrix4<>& view) const;

	private:
		std::vector<std::shared_ptr<Mesh>> _meshes;
	};
}