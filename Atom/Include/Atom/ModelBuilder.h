#pragma once
#include "Atom/Model.h"
#include "Atom/MeshBuilder.h"

namespace Atom {
	class ATOM_API ModelBuilder {
	public:
		bool load(const char* path, std::shared_ptr<Shader> shader);

		std::shared_ptr<Model> build();

	private:
		void mergeMeshes(const std::vector<std::shared_ptr<Mesh>>& meshes);

		std::vector<std::shared_ptr<Mesh>> _meshes;
	};
}