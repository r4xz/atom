#include "Atom/API.h"
#include "Atom/Mesh.h"

namespace Atom {
	class ATOM_API ModelParser {
	public:
		virtual bool parse(const char* path, std::shared_ptr<Shader> shader) = 0;
		const std::vector<std::shared_ptr<Mesh>>& result() const;

	protected:
		std::vector<std::shared_ptr<Mesh>> _meshes;
	};
}