#pragma once
#include "Atom/API.h"
#include <cassert>

namespace Atom {
	template <class T>
	class ATOM_API Nullable {
	public:
		// Initialize without value.
		Nullable() : _exists(false) {}

		// Initialize with value.
		// @param value - Starting value.
		Nullable(const T& value) : _value(value), _exists(true) {}

		// Check whether any value is stored.
		// @return True if contain value, false otherwise.
		bool hasValue() const {
			return _exists;
		}

		// Get current value.
		// @return Stored value.
		const T& value() const {
			assert(_exists);

			return _value;
		}

		// Get current value.
		// @return Stored value.
		const T* operator->() const {
			assert(_exists);

			return &_value;
		}

		// Set new or replace old value.
		// @param value - New value to be stored.
		void setValue(const T& value) {
			_exists = true;
			_value = value;
		}

		// Mark container as empty
		// (stored value will not be destroyed).
		void setNull() {
			_exists = false;
		}

		// Check whether any value is stored.
		operator bool() const {
			return _exists;
		}

		// Check whether any value is stored.
		// @return True if contain value, false otherwise.
		bool operator!() const {
			return !_exists;
		}

	private:
		bool _exists;
		T _value;
	};
}