#include "Atom/ModelParser.h"
#include "Atom/MeshBuilder.h"

namespace Atom {
	class ATOM_API ObjParser : public ModelParser {
	public:
		virtual bool parse(const char* path, std::shared_ptr<Shader> shader);

	private:
		MeshBuilder _mesh;

		bool parseVertex(const char* line);
		bool parseTexCoord(const char* line);
		bool parseNormal(const char* line);
		bool parseFace(const char* line);
	};
}