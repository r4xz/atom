#pragma once
#include "Atom/Matrix4.h"

namespace Atom {
	class ATOM_API Object {
		friend class Pipeline;

	public:
		// Get object position in 3D space.
		// @return Object position vector (xyz).
		Vector3<> position() const;

		// Get object transformation (rotate, scale, translate).
		// @return Matrix4 transformation.
		const Matrix4<>& transformation() const;

	protected:
		// Draw object to active framebuffer.
		// @param projection - Projection matrix.
		// @param view - Camera transformation matrix.
		virtual void render(const Matrix4<>& projection, const Matrix4<>& view) const = 0;

		// Allow user-defined destructor.
		virtual ~Object() {}

		Matrix4<> _local;
	};
}