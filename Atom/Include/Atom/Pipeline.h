#pragma once
#include "Atom/Camera.h"
#include <memory>

namespace Atom {
	class ATOM_API Pipeline {
	public:
		// TODO: before render catch canvas size from framebuffer object (global instance for window)
		void render(std::shared_ptr<const Camera> camera, std::shared_ptr<const Object> scene);
	};
}