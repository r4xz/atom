#pragma once
#include "Atom/ShaderSource.h"
#include <memory>

namespace Atom {
	class ATOM_API Shader {
	public:
		Shader();
		~Shader();

		typedef GLint Location;

		// Undefined variable, common reasons:
		// - named variable is not active
		// - name starts with the reserved prefix "gl_"
		static const Location InvalidLocation = -1;

		// Attach source to the shader program
		// (changes take effect after linking process).
		// @param source - Source to be attached.
		void attach(const ShaderSource& source);

		// Create program from linked sources
		// (unknown shader status if linking process fail).
		// @return True if success, false otherwise.
		bool link();

		// Get shader attribute location
		// (it's recommended to retrieve variable fixed number of times).
		// @param name - GLSL attribute name.
		// @return Variable location or 'InvalidLocation' if not exists.
		Location attribute(const GLchar* name) const;

		// Get shader uniform location
		// (it's recommended to retrieve variable fixed number of times).
		// @param name - GLSL uniform name.
		// @return Variable location or 'InvalidLocation' if not exists.
		Location uniform(const GLchar* name) const;

		// Make shader program active.
		void use() const;

	private:
		GLuint _glProgram;
	};
}