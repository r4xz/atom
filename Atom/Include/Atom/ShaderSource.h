#pragma once
#include "Atom/Context.h"

namespace Atom {
	class ATOM_API ShaderSource {
		friend class Shader;

	public:
		enum Type {
			Vertex = GL_VERTEX_SHADER,
			TessControl = GL_TESS_CONTROL_SHADER,
			TessEvaluation = GL_TESS_EVALUATION_SHADER,
			Geometry = GL_GEOMETRY_SHADER,
			Fragment = GL_FRAGMENT_SHADER,
			Compute = GL_COMPUTE_SHADER
		};

		ShaderSource();
		~ShaderSource();

		// Compile shader source file.
		// @param path - Path to the source file.
		// @param type - Shader source type.
		// @return True if success, false otherwise.
		bool compile(const char* path, Type type);

		// Mark object as destroyed.
		// GPU memory will be free along with the
		// removal of the last attached shader program.
		void dispose();

	private:
		GLuint _glShader;
	};
}