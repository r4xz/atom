#include "Atom/Shader.h"
#include "Atom/Matrix4.h"
#include "Atom/Vector2.h"
#include "Atom/Vector3.h"
#include "Atom/Color.h"

namespace Atom {
	class ATOM_API ShaderUniform {
	public:
		// Set uniform color (operates on the active shader).
		// @param uniform - Uniform location.
		// @param color - New uniform color value.
		static void setColor(Shader::Location uniform, Color color);

		// Set uniform value (operates on the active shader).
		// @param uniform - Uniform location.
		// @param val - New uniform value.
		static void setValue(Shader::Location uniform, int val);

		// Set uniform value (operates on the active shader).
		// @param uniform - Uniform location.
		// @param val - New uniform value.
		static void setValue(Shader::Location uniform, uint val);

		// Set uniform value (operates on the active shader).
		// @param uniform - Uniform location.
		// @param val - New uniform value.
		static void setValue(Shader::Location uniform, float val);

		// Set uniform vector (operates on the active shader).
		// @param uniform - Uniform location.
		// @param vec - New uniform vector.
		static void setVector(Shader::Location uniform, const Vector2<>& vec);

		// Set uniform vector (operates on the active shader).
		// @param uniform - Uniform location.
		// @param vec - New uniform vector.
		static void setVector(Shader::Location uniform, const Vector3<>& vec);

		// Set uniform vector (operates on the active shader).
		// @param uniform - Uniform location.
		// @param vec - New uniform vector.
		static void setVector(Shader::Location uniform, const Vector4<>& vec);

		// Set uniform matrix (operates on the active shader).
		// @param uniform - Uniform location.
		// @param mat - New uniform matrix.
		static void setMatrix(Shader::Location uniform, const Matrix4<>& mat);
	};
}