#include "Atom/API.h"
#include <cassert>

namespace Atom {
	template<class T>
	class ATOM_API Size {
	public:
		T width;
		T height;

		Size() : width(0), height(0) {};
		Size(T width, T height) : width(width), height(height) {};

		// Get square area.
		// @return Non-negative area.
		T area() {
			assert(width >= 0 && height >= 0);

			return (width * height);
		}
	};
}