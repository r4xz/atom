#pragma once
#include "Atom/Material.h"
#include "Atom/Color.h"

namespace Atom {
	// TODO: Uniforms: projection, model, view
	class ATOM_API SolidMaterial : public Material {
	public:
		// Material color.
		Color color;

		// Create material.
		// @param shader - Shader used by material.
		SolidMaterial(std::shared_ptr<const Shader> shader);

		// Active shader and update uniforms values.
		virtual void use(const Matrix4<>& projection, const Matrix4<>& view, const Matrix4<>& model) const;

	protected:
		// Initialize material.
		// Cache required shader attributes and uniforms loctions.
		virtual void initialize();

	private:
		Shader::Location _colorUniform;
	};
}