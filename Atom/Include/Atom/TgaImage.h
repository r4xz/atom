#pragma once
#include "Atom/Image.h"
#include "Atom/Types.h"
#include "Atom/Nullable.h"
#include "Atom/FileReader.h"

namespace Atom {
	// TGA file format image.
	class ATOM_API TgaImage : public Image {
	public:
		TgaImage();
		~TgaImage();

		// Get/Set image pixel.
		// @param x, y - Pixel coordinate, upper left corner is point (0,0).
		// @return Bitmap pixel reference.
		virtual Color& operator()(uint x, uint y);

		// Get image size.
		// @return Zero values if the image does non exists, positive numbers otherwise.
		virtual Size<uint> size() const;

		// Create blank bitmap of specified size.
		// @param size - Bitmap size in pixels.
		virtual void create(Size<uint> size);

		// Load image from file.
		// Support non-interleaved & unmapped RGBA with origin in upper left-hand corner.
		// @param path - Path to the file.
		// @return True if successfully loaded, false otherwise.
		virtual bool load(const char* path);

		// Set uniform color of the entire image.
		// @param color - Bitmap color.
		virtual void repaint(Color color);

		// Destroy bitmap.
		virtual void dispose();

	protected:
		typedef struct {
			uchar idLength;
			uchar colorMapType;
			uchar dataTypeCode;
			ushort colorMapOrigin;
			ushort colorMapLength;
			uchar colorMapDepth;
			ushort xOrigin;
			ushort yOrigin;
			ushort width;
			ushort height;
			uchar bitsPerPixel;
			uchar imageDescriptor;
		} Header;

		Size<uint> _size;
		Color* _bitmap;

		// Read structure of the file header.
		// @param stream - Opened file stream.
		// @return Null if format is not supported, TGA header otherwise.
		static Nullable<Header> readHeader(FileReader& reader);

		// Check whether header is supported by parser.
		// @param header - TGA fileformat header to be checked.
		// @return True if supported, false otherwise.
		static bool isHeaderSupported(const Header& header);

		// Check whether file footer is correct.
		// @param stream - Opened file stream.
		// @return True if correct, false otherwise.
		static bool isFooterCorrect(FileReader& reader);
	};
}