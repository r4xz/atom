#pragma once
#include "Atom/Context.h"
#include "Atom/Vector3.h"

namespace Atom {
	template <class T = GLfloat>
	class ATOM_API Vector2 {
	public:
		union { T x, u; };
		union { T y, v; };

		// Zero vector.
		Vector2() : x(0), y(0) {}

		// Custom vector.
		// @param x, y - XY/UV values.
		Vector2(T x, T y) : x(x), y(y) {}

		Vector2(Vector3<T> v) : x(v.x), y(v.y) {}

		void operator+=(const T& rht) {
			x += rht;
			y += rht;
		}

		void operator+=(const Vector2<T>& rht) {
			x += rht.x;
			y += rht.y;
		}

		void operator-=(const T& rht) {
			x -= rht;
			y -= rht;
		}

		void operator-=(const Vector2<T>& rht) {
			x -= rht.x;
			y -= rht.y;
		}

		void operator*=(const T& rht) {
			x *= rht;
			y *= rht;
		}

		void operator*=(const Vector2<T>& rht) {
			x *= rht.x;
			y *= rht.y;
		}

		void operator/=(const T& rht) {
			x /= rht;
			y /= rht;
		}

		void operator/=(const Vector2<T>& rht) {
			x /= rht.x;
			y /= rht.y;
		}

		Vector2<T> operator+(const T& rht) const {
			return Vector2<T>(x + rht, y + rht);
		}

		Vector2<T> operator+(const Vector2<T>& rht) const {
			return Vector2<T>(x + rht.x, y + rht.y);
		}

		Vector2<T> operator-(const T& rht) const {
			return Vector2<T>(x - rht, y - rht);
		}

		Vector2<T> operator-(const Vector2<T>& rht) const {
			return Vector2<T>(x - rht.x, y - rht.y);
		}

		Vector2<T> operator*(const T& rht) const {
			return Vector2<T>(x * rht, y * rht);
		}

		Vector2<T> operator*(const Vector2<T>& rht) const {
			return Vector2<T>(x * rht.x, y * rht.y);
		}

		Vector2<T> operator/(const T& rht) const {
			return Vector2<T>(x / rht, y / rht);
		}

		Vector2<T> operator/(const Vector2<T>& rht) const {
			return Vector2<T>(x / rht.x, y / rht.y);
		}

		// Compare two vectors.
		// @param v - Vector to compare with.
		// @return True if vectors are equal, false otherwise.
		bool operator==(const Vector2<T>& v) const {
			return (x == v.x && y == v.y);
		}

		// Compare two vectors.
		// @param v - Vector to compare with.
		// @return False if vectors are equal, true otherwise.
		bool operator!=(const Vector2<T>& v) const {
			return (x != v.x || y != v.y);
		}

		// Get normalized vector.
		// @return Normalized vector.
		Vector2<T> normalized() const {
			T div = length();
			return Vector2<T>(x / div, y / div);
		}

		// Get vector length.
		// @return Non-negative length.
		T length() const {
			return sqrt(x*x + y*y);
		}

		// Get vector squared length
		// (much faster that length method).
		// @return Non-negative length.
		T magnitude() const {
			return x*x + y*y;
		}

		// Dot product of two vectors.
		// For normalized vectors Dot returns 1 if they point in exactly the same direction,
		// -1 if they point in completely opposite directions, and a number in between
		// for other cases (e.g. Dot returns zero if vectors are perpendicular).
		// @param rht - Right vector operand.
		// @return Dot product this . rht.
		T dot(const Vector2<T>& rht) const {
			return (x * rht.x) + (y * rht.y);
		}
	};
}