#pragma once
#include "Atom/Context.h"
#include "Atom/Vector4.h"
#include <cmath>

namespace Atom {
	template <class T = GLfloat>
	class ATOM_API Vector3 {
	public:
		union { T x, r; };
		union { T y, g; };
		union { T z, b; };

		// Zero vector
		Vector3() : x(0), y(0), z(0) {}

		// Custom vector.
		// @param x, y, z - XYZ/RGB values.
		Vector3(T x, T y, T z) : x(x), y(y), z(z) {}

		Vector3(Vector4<T> v) : x(v.x), y(v.y), z(v.z) {}

		void operator+=(const T& rht) {
			x += rht;
			y += rht;
			z += rht;
		}

		void operator+=(const Vector3<T>& rht) {
			x += rht.x;
			y += rht.y;
			z += rht.z;
		}

		void operator-=(const T& rht) {
			x -= rht;
			y -= rht;
			z -= rht;
		}

		void operator-=(const Vector3<T>& rht) {
			x -= rht.x;
			y -= rht.y;
			z -= rht.z;
		}

		void operator*=(const T& rht) {
			x *= rht;
			y *= rht;
			z *= rht;
		}

		void operator*=(const Vector3<T>& rht) {
			x *= rht.x;
			y *= rht.y;
			z *= rht.z;
		}

		void operator/=(const T& rht) {
			x /= rht;
			y /= rht;
			z /= rht;
		}

		void operator/=(const Vector3<T>& rht) {
			x /= rht.x;
			y /= rht.y;
			z /= rht.z;
		}

		Vector3<T> operator+(const T& rht) const {
			return Vector3<T>(x + rht, y + rht, z + rht);
		}

		Vector3<T> operator+(const Vector3<T>& rht) const {
			return Vector3<T>(x + rht.x, y + rht.y, z + rht.z);
		}

		Vector3<T> operator-(const T& rht) const {
			return Vector3<T>(x - rht, y - rht, z - rht);
		}

		Vector3<T> operator-(const Vector3<T>& rht) const {
			return Vector3<T>(x - rht.x, y - rht.y, z - rht.z);
		}

		Vector3<T> operator*(const T& rht) const {
			return Vector3<T>(x * rht, y * rht, z * rht);
		}

		Vector3<T> operator*(const Vector3<T>& rht) const {
			return Vector3<T>(x * rht.x, y * rht.y, z * rht.z);
		}

		Vector3<T> operator/(const T& rht) const {
			return Vector3<T>(x / rht, y / rht, z / rht);
		}

		Vector3<T> operator/(const Vector3<T>& rht) const {
			return Vector3<T>(x / rht.x, y / rht.y, z / rht.z);
		}

		// Compare two vectors.
		// @param v - Vector to compare with.
		// @return True if vectors are equal, false otherwise.
		bool operator==(const Vector3<T>& v) const {
			return (x == v.x && y == v.y && z == v.z);
		}

		// Compare two vectors.
		// @param v - Vector to compare with.
		// @return False if vectors are equal, true otherwise.
		bool operator!=(const Vector3<T>& v) const {
			return (x != v.x || y != v.y || z != v.z);
		}

		// Get normalized vector.
		// @return Normalized vector.
		Vector3<T> normalized() const {
			T div = length();
			return Vector3<T>(x / div, y / div, z / div);
		}

		// Get vector length.
		// @return Non-negative length.
		T length() const {
			return sqrt(x*x + y*y + z*z);
		}

		// Get vector squared length
		// (much faster that length method).
		// @return Non-negative length.
		T magnitude() const {
			return x*x + y*y + z*z;
		}

		// Dot product of two vectors.
		// For normalized vectors Dot returns 1 if they point in exactly the same direction,
		// -1 if they point in completely opposite directions, and a number in between
		// for other cases (e.g. Dot returns zero if vectors are perpendicular).
		// @param rht - Right vector operand.
		// @return Dot product this . rht.
		T dot(const Vector3<T>& rht) const {
			return (x * rht.x) + (y * rht.y) + (z * rht.z);
		}

		// Cross product of two vectors.
		// @param rht - Right vector operand.
		// @return Cross product this x rht.
		Vector3<T> cross(const Vector3<T>& rht) const {
			return Vector3<T>(
				(y * rht.z) - (z * rht.y),
				(z * rht.x) - (x * rht.z),
				(x * rht.y) - (y * rht.x)
			);
		}
	};
}