#pragma once
#include "Atom/Context.h"

namespace Atom {
	// Column, 4 dimensional vector.
	template <class T = GLfloat>
	class ATOM_API Vector4 {
	public:
		union { T x, r; };
		union { T y, g; };
		union { T z, b; };
		union { T w, a; };

		// Zero vector
		Vector4() : x(0), y(0), z(0), w(0) {}

		// Custom vector.
		// @param x, y, z, w - XYZW/RGBA values.
		Vector4(T x, T y, T z, T w) : x(x), y(y), z(z), w(w) {}

		void operator+=(const T& rht) {
			x += rht;
			y += rht;
			z += rht;
			w += rht;
		}

		void operator+=(const Vector4<T>& rht) {
			x += rht.x;
			y += rht.y;
			z += rht.z;
			w += rht.w;
		}

		void operator-=(const T& rht) {
			x -= rht;
			y -= rht;
			z -= rht;
			w -= rht;
		}

		void operator-=(const Vector4<T>& rht) {
			x -= rht.x;
			y -= rht.y;
			z -= rht.z;
			w -= rht.w;
		}

		void operator*=(const T& rht) {
			x *= rht;
			y *= rht;
			z *= rht;
			w *= rht;
		}

		void operator*=(const Vector4<T>& rht) {
			x *= rht.x;
			y *= rht.y;
			z *= rht.z;
			w *= rht.w;
		}

		void operator/=(const T& rht) {
			x /= rht;
			y /= rht;
			z /= rht;
			w /= rht;
		}

		void operator/=(const Vector4<T>& rht) {
			x /= rht.x;
			y /= rht.y;
			z /= rht.z;
			w /= rht.w;
		}

		Vector4<T> operator+(const T& rht) const {
			return Vector4<T>(x + rht, y + rht, z + rht, w + rht);
		}

		Vector4<T> operator+(const Vector4<T>& rht) const {
			return Vector4<T>(x + rht.x, y + rht.y, z + rht.z, w + rht.w);
		}

		Vector4<T> operator-(const T& rht) const {
			return Vector4<T>(x - rht, y - rht, z - rht, w - rht);
		}

		Vector4<T> operator-(const Vector4<T>& rht) const {
			return Vector4<T>(x - rht.x, y - rht.y, z - rht.z, w - rht.w);
		}

		Vector4<T> operator*(const T& rht) const {
			return Vector4<T>(x * rht, y * rht, z * rht, w * rht);
		}

		Vector4<T> operator*(const Vector4<T>& rht) const {
			return Vector4<T>(x * rht.x, y * rht.y, z * rht.z, w * rht.w);
		}

		Vector4<T> operator/(const T& rht) const {
			return Vector4<T>(x / rht, y / rht, z / rht, w / rht);
		}

		Vector4<T> operator/(const Vector4<T>& rht) const {
			return Vector4<T>(x / rht.x, y / rht.y, z / rht.z, w / rht.w);
		}

		// Compare two vectors.
		// @param v - Vector to compare with.
		// @return True if vectors are equal, false otherwise.
		bool operator==(const Vector4<T>& v) const {
			return (x == v.x && y == v.y && z == v.z && w == v.w);
		}

		// Compare two vectors.
		// @param v - Vector to compare with.
		// @return False if vectors are equal, true otherwise.
		bool operator!=(const Vector4<T>& v) const {
			return (x != v.x || y != v.y || z != v.z || w != v.w);
		}

		// Get normalized vector.
		// @return Normalized vector.
		Vector4<T> normalized() const {
			T div = length();
			return Vector4<T>(x / div, y / div, z / div, w / div);
		}

		// Get vector length.
		// @return Non-negative length.
		T length() const {
			return sqrt(x*x + y*y + z*z + w*w);
		}

		// Get vector squared length
		// (much faster that length method).
		// @return Non-negative length.
		T magnitude() const {
			return x*x + y*y + z*z + w*w;
		}

		// Dot product of two vectors.
		// For normalized vectors Dot returns 1 if they point in exactly the same direction,
		// -1 if they point in completely opposite directions, and a number in between
		// for other cases (e.g. Dot returns zero if vectors are perpendicular).
		// @param rht - Right vector operand.
		// @return Dot product this . rht.
		T dot(const Vector4<T>& rht) const {
			return (x * rht.x) + (y * rht.y) + (z * rht.z) + (w * rht.w);
		}
	};
}