#define _USE_MATH_DEFINES
#include "Atom/Angle.h"
#include <cmath>

namespace Atom {
	Angle Angle::deg(float angle) {
		return Angle(angle);
	}

	Angle Angle::rad(float angle) {
		return Angle(toDeg(angle));
	}

	float Angle::deg() {
		return _deg;
	}

	float Angle::rad() {
		return _rad;
	}

	float Angle::toDeg(float rad) {
		return (float)(rad * 180.0f / M_PI);
	}

	float Angle::toRad(float deg) {
		return (float)(deg * M_PI / 180.0f);
	}

	Angle::Angle(float deg) {
		_deg = deg;
		_rad = toRad(deg);
	}
}