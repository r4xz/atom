#include "Atom/Camera.h"

namespace Atom {
	Camera::Camera(Type type) {
		this->type = type;
		// FIXME: lookAt works strange :)
		_local = Matrix4<>::lookAt(Vector3<>(0, 0, 0), Vector3<>(0, 0, -1), Vector3<>(0, 1, 0));
	}

	void Camera::render(const Matrix4<>& projection, const Matrix4<>& view) const {}
}