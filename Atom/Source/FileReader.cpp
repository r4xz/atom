#include "Atom/FileReader.h"
#include <cassert>

namespace Atom {
	FileReader::FileReader() : _buffer(NULL), _buflen(0), FileStream() {}

	FileReader::~FileReader() {
		dispose();
	}

	bool FileReader::open(const char* path) {
		_stream = fopen(path, "rb");
		setCursor(Cursor::Begin);

		return isOpen();
	}

	bool FileReader::readAll() {
		assert(isOpen());

		prepareBuffer(length());
		return fread(_buffer, sizeof(char), length(), _stream) == length();
	}

	const char* FileReader::getAll() {
		if (!readAll())
			_buffer[0] = 0;

		return _buffer;
	}

	bool FileReader::readBlock(long size) {
		assert(isOpen());

		prepareBuffer(size);
		return fread(_buffer, sizeof(char), size, _stream) == size;
	}

	const char* FileReader::getBlock(long size) {
		if (!readBlock(size))
			_buffer[0] = 0;

		return _buffer;
	}

	bool FileReader::readLine(long breakAt) {
		assert(isOpen());

		prepareBuffer(breakAt);
		return fgets(_buffer, breakAt, _stream) != NULL;
	}

	const char* FileReader::getLine(long breakAt) {
		if (!readLine(breakAt))
			_buffer[0] = 0;

		return _buffer;
	}

	bool FileReader::readChar() {
		assert(isOpen());

		prepareBuffer(1);
		return fread(_buffer, sizeof(char), 1, _stream) == 1;
	}

	uchar FileReader::getChar() {
		if (!readChar())
			_buffer[0] = 0;

		return (uchar) _buffer[0];
	}

	const char* FileReader::buffer() const {
		return _buffer;
	}

	void FileReader::dispose() {
		if (_buffer != NULL) {
			delete[] _buffer;
			_buffer = NULL;
			_buflen = 0;
		}
	}

	void FileReader::prepareBuffer(long size) {
		assert(size > 0);

		// Not enough buffer size
		// (including zero character).
		if (_buflen <= size) {
			dispose();
		}

		// Allocate new buffer.
		if (_buflen <= 0) {
			_buflen = size + 1;
			_buffer = new char[_buflen];
		}

		// Secure string with zero character.
		_buffer[size] = 0;
	}
}