#include "Atom/FileStream.h"
#include <cassert>

namespace Atom {
	FileStream::FileStream() : _stream(NULL), _length(0) {}

	FileStream::~FileStream() {
		close();
	}

	bool FileStream::isOpen() const {
		return _stream != NULL;
	}

	long FileStream::cursor() const {
		assert(isOpen());

		return ftell(_stream);
	}

	bool FileStream::setCursor(Cursor origin, long offset) {
		assert(isOpen());

		return fseek(_stream, offset, origin) == 0;
	}

	long FileStream::length() const {
		// Return buffered value
		if (_length > 0)
			return _length;

		// Backup cursor position
		FILE* stream = (FILE*)_stream;
		long cursor = ftell(stream);

		// Get file size
		fseek(stream, 0, Cursor::End);
		long _length = ftell(stream);

		// Restore cursor
		fseek(stream, cursor, Cursor::Begin);
		return _length;
	}

	void FileStream::close() {
		if (_stream != NULL) {
			fclose(_stream);
			_stream = NULL;
		}
	}
}