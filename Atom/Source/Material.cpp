#include "Atom/Material.h"

namespace Atom {
	Material::Material(std::shared_ptr<const Shader> shader) : _shader(shader) {}

	Shader::Location Material::vertexAttrib() const {
		return _vertexAttrib;
	}

	Shader::Location Material::normalAttrib() const {
		return _normalAttrib;
	}

	Shader::Location Material::texCoordAttrib() const {
		return _texCoordAttrib;
	}

	Shader::Location Material::mvpUniform() const {
		return _mvpUniform;
	}
}