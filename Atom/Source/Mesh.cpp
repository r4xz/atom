#include "Atom/Mesh.h"

namespace Atom {
	Mesh::Mesh(GLuint vao, const std::vector<GLuint>& buffers, const std::vector<uint>& indices, std::shared_ptr<const Material> material) : _vao(vao), _buffers(buffers), _indices(indices), _material(material) {}

	void Mesh::render(const Matrix4<>& projection, const Matrix4<>& view) const {
		glBindVertexArray(_vao);

		_material->use(projection, view, transformation());
		glDrawElements(GL_TRIANGLES, _indices.size(), GL_UNSIGNED_INT, 0);

		glBindVertexArray(0);
	}

	Mesh::~Mesh() {
		dispose();
	}

	void Mesh::dispose() {
		glDeleteBuffers(_buffers.size(), &_buffers[0]);
		glDeleteVertexArrays(1, &_vao);
	}
}