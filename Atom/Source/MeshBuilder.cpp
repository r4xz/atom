#include "Atom/MeshBuilder.h"
#include <cassert>
#include <map>

namespace Atom {
	bool operator<(const MeshBuilder::Indice& lft, const MeshBuilder::Indice& rht) {
		return memcmp(&lft, &rht, sizeof(MeshBuilder::Indice)) < 0;
	}

	MeshBuilder::MeshBuilder() {
		clear();
	}

	int MeshBuilder::addVertex(Vector3<> vertex) {
		_vertices.push_back(vertex);
		return verticesCount() - 1;
	}

	int MeshBuilder::verticesCount() const {
		return (int)_vertices.size();
	}

	int MeshBuilder::addNormal(Vector3<> normal) {
		_normals.push_back(normal);
		return normalsCount() - 1;
	}

	int MeshBuilder::normalsCount() const {
		return (int) _normals.size();
	}

	int MeshBuilder::addTexCoord(Vector2<> texCoord) {
		_texCoords.push_back(texCoord);
		return texCoordsCount() - 1;
	}

	int MeshBuilder::texCoordsCount() const {
		return (int)_texCoords.size();
	}

	void MeshBuilder::addIndice(Indice indice) {
		assert(indice.vertex < verticesCount());
		assert(indice.normal < normalsCount());
		assert(indice.texCoord < texCoordsCount());

		if (indice.vertex < 0)
			indice.vertex += verticesCount();

		if (indice.normal < 0)
			indice.normal += normalsCount();

		if (indice.texCoord < 0)
			indice.texCoord += texCoordsCount();

		_indices.push_back(indice);
	}

	uint MeshBuilder::indicesCount() const {
		return _indices.size();
	}

	void MeshBuilder::addFace(Indice i1, Indice i2, Indice i3) {
		addIndice(i1);
		addIndice(i2);
		addIndice(i3);
	}

	std::vector<uint> MeshBuilder::compact() {
		std::vector<uint> resultIndices;

		// New containers.
		std::vector<Vector3<>> packedVertices(1);
		std::vector<Vector3<>> packedNormals(1);
		std::vector<Vector2<>> packedTexCoords(1);
		std::vector<Indice> packedIndices;

		// Rearrange indices.
		uint indexCounter = 1;
		std::map<Indice, uint> indicesMap;
		for (Indice i : _indices) {
			auto key = indicesMap.find(i);

			// First indice occurrence.
			if (key == indicesMap.end()) {
				indicesMap.insert({ i, indexCounter });

				packedVertices.push_back(_vertices[i.vertex]);
				packedNormals.push_back(_normals[i.normal]);
				packedTexCoords.push_back(_texCoords[i.texCoord]);

				key = indicesMap.find(i);
				indexCounter++;
			}
			
			// Add indice.
			uint value = key->second;
			resultIndices.push_back(value);
			packedIndices.push_back({ value, value, value });
		}

		// Swap containers.
		_vertices = packedVertices;
		_normals = packedNormals;
		_texCoords = packedTexCoords;
		_indices = packedIndices;

		return resultIndices;
	}

	void MeshBuilder::clear() {
		_vertices.clear();
		_normals.clear();
		_texCoords.clear();
		_indices.clear();

		_vertices.push_back({ 0, 0, 0 });
		_normals.push_back({ 0, 0, 0 });
		_texCoords.push_back({ 0, 0 });
	}

	void MeshBuilder::clearIndices() {
		_indices.clear();
	}

	std::shared_ptr<Mesh> MeshBuilder::build(std::shared_ptr<const Material> material) {
		assert(indicesCount() % 3 == 0); // 3 indices = triangle

		// Generate VAO.
		GLuint vao;
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);

		std::vector<GLuint> buffers;

		// Arrange containers to the OpenGL standard
		// (and optimize large meshes).
		std::vector<uint> indices = compact();

		// Generate vertices buffer
		Shader::Location vertexAttrib = material->vertexAttrib();
		if (vertexAttrib != Shader::InvalidLocation && verticesCount() > 1) {
			GLuint buffer;

			glGenBuffers(1, &buffer);
			glBindBuffer(GL_ARRAY_BUFFER, buffer);
			glBufferData(GL_ARRAY_BUFFER, sizeof(_vertices[0])*verticesCount(), &_vertices[0], GL_STATIC_DRAW);

			glEnableVertexAttribArray(buffers.size());
			glVertexAttribPointer(vertexAttrib, 3, GL_FLOAT, GL_FALSE, 0, 0);

			buffers.push_back(buffer);
		}

		// Generate normals buffer
		Shader::Location normalAttrib = material->normalAttrib();
		if (normalAttrib != Shader::InvalidLocation && normalsCount() > 1) {
			GLuint buffer;

			glGenBuffers(1, &buffer);
			glBindBuffer(GL_ARRAY_BUFFER, buffer);
			glBufferData(GL_ARRAY_BUFFER, sizeof(_normals[0])*normalsCount(), &_normals[0], GL_STATIC_DRAW);

			glEnableVertexAttribArray(buffers.size());
			glVertexAttribPointer(normalAttrib, 3, GL_FLOAT, GL_TRUE, 0, 0);

			buffers.push_back(buffer);
		}

		// Generate texture coordinates buffer
		Shader::Location texCoordAttrib = material->texCoordAttrib();
		if (texCoordAttrib != Shader::InvalidLocation && texCoordsCount() > 1) {
			GLuint buffer;

			glGenBuffers(1, &buffer);
			glBindBuffer(GL_ARRAY_BUFFER, buffer);
			glBufferData(GL_ARRAY_BUFFER, sizeof(_texCoords[0])*texCoordsCount(), &_texCoords[0], GL_STATIC_DRAW);

			glEnableVertexAttribArray(buffers.size());
			glVertexAttribPointer(texCoordAttrib, 2, GL_FLOAT, GL_FALSE, 0, 0);
			
			buffers.push_back(buffer);
		}

		// Generate indices buffer
		if (vertexAttrib != Shader::InvalidLocation) {
			GLuint buffer;

			glGenBuffers(1, &buffer);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices[0])*indicesCount(), &indices[0], GL_STATIC_DRAW);

			buffers.push_back(buffer);
		}

		// Make sure that VAO is not changed from outside the code.
		glBindVertexArray(0);
		
		return std::make_shared<Mesh>(vao, buffers, indices, material);
	}
}