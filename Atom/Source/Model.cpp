#include "Atom/Model.h"

namespace Atom {
	Model::Model(std::vector<std::shared_ptr<Mesh>> meshes) : _meshes(meshes) {}

	void Model::render(const Matrix4<>& projection, const Matrix4<>& view) const {
		for (std::shared_ptr<Mesh> mesh : _meshes)
			mesh->render(projection, view);
	}
}