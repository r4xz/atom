#include "Atom/ModelBuilder.h"
#include "Atom/ObjParser.h"

namespace Atom {
	bool ModelBuilder::load(const char* path, std::shared_ptr<Shader> shader) {
		const char* ext = strrchr(path, '.');

		// Load supported model files
		if (strcmp(ext, ".obj") == 0) {
			ObjParser obj;
			if (!obj.parse(path, shader))
				return false;

			mergeMeshes(obj.result());
			return true;
		}
		
		// File is not supported
		fprintf(stderr, "Unsupported model format (determined using file extension)");
		return false;
	}

	std::shared_ptr<Model> ModelBuilder::build() {
		return std::make_shared<Model>(_meshes);
	}

	void ModelBuilder::mergeMeshes(const std::vector<std::shared_ptr<Mesh>>& meshes) {
		for (auto mesh : meshes)
			_meshes.push_back(mesh);
	}
}