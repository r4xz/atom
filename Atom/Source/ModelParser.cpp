#include "Atom/ModelParser.h"

namespace Atom {
	const std::vector<std::shared_ptr<Mesh>>& ModelParser::result() const {
		return _meshes;
	}
}