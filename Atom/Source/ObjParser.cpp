#include "Atom/ObjParser.h"
#include "Atom/FileReader.h"
#include "Atom/SolidMaterial.h"

namespace Atom {
	bool ObjParser::parse(const char* path, std::shared_ptr<Shader> shader) {
		// Check if file exists
		FileReader reader;
		if (!reader.open(path))
			return false;

		// Parse line-by-line
		while (reader.readLine()) {
			const char* line = reader.buffer();

			switch (line[0]) {
			case '#':
			case '\0':
				break;

			case 'v':
				switch (line[1]) {
				case ' ':
					if (!parseVertex(line)) {
						fprintf(stderr, "Invalid vertex format. Expected 'v <x> <y> <z> [<w>]', got '%s'", line);
						return false;
					}
					break;

				case 't':
					if (!parseTexCoord(line)) {
						fprintf(stderr, "Invalid texture coord format. Expected 'vt <u> [<v> [<w>]]', got '%s'", line);
						return false;
					}
					break;

				case 'n':
					if (!parseNormal(line)) {
						fprintf(stderr, "Invalid normal format. Expected 'vn <x> <y> <z>', got '%s'", line);
						return false;
					}
					break;
				}
				break;

			case 'f':
				if (!parseFace(line)) {
					fprintf(stderr, "Invalid face format. Expected 'f (<v>/[<vt>][/<vn>]){3,}', got '%s'", line);
					return false;
				}
				break;

			case 'g':
			case 'o':
				if (_mesh.indicesCount() > 0) {
					// TODO: Model material created from .mtl file
					// (shader still specified by user)
					auto tmpMaterial = std::make_shared<SolidMaterial>(shader);
					tmpMaterial->color = Color(255, 0, 0, 255);

					// TODO: mesh.build should not change containers
					MeshBuilder mesh(_mesh);
					_meshes.push_back(mesh.build(tmpMaterial));
					_mesh.clearIndices();
				}
				break;
			}
		}

		// Build last object
		auto tmpMaterial = std::make_shared<SolidMaterial>(shader);
		tmpMaterial->color = Color(255, 0, 0, 255);

		_meshes.push_back(_mesh.build(tmpMaterial));

		return true;
	}

	bool ObjParser::parseVertex(const char* line) {
		Vector4<> v(0, 0, 0, 1);
		if (sscanf(line, "v %f %f %f %f", &v.x, &v.y, &v.z, &v.w) < 3)
			return false;

		_mesh.addVertex(v/v.w);
		return true;
	}

	bool ObjParser::parseTexCoord(const char* line) {
		Vector3<> v(0, 0, 0);
		if (sscanf(line, "vt %f %f %f", &v.x, &v.y, &v.z) < 1)
			return false;

		_mesh.addTexCoord(v/v.z);
		return true;
	}

	bool ObjParser::parseNormal(const char* line) {
		Vector3<> v(0, 0, 0);
		if (sscanf(line, "vn %f %f %f", &v.x, &v.y, &v.z) < 3)
			return false;

		_mesh.addNormal(v);
		return true;
	}

	bool ObjParser::parseFace(const char* line) {
		std::vector<MeshBuilder::Indice> indices;

		// Empty string protection
		const char* it = line + 2;
		if (line[0] == NULL || line[1] == NULL)
			return false;

		// Parse face point-by-point (triangle fan)
		int i = 0;
		char buf[255];
		MeshBuilder::Indice v = { 0, 0, 0 };
		while (*it != '\0') {
			sscanf(it, "%255s", buf);
			it += strlen(buf) + 1;

			if (sscanf(buf, "%d/%d/%d", &v.vertex, &v.texCoord, &v.normal) == 3);
			else if (sscanf(buf, "%d//%d", &v.vertex, &v.normal) == 2);
			else if (sscanf(buf, "%d/%d", &v.vertex, &v.texCoord) == 2);
			else if(sscanf(buf, "%d", &v.vertex) == 1);
			else return false;

			++i;
			if (i > 3) {
				indices.push_back(indices[0]);
				indices.push_back(indices[i-2]);
				indices.push_back(v);
			}
			else {
				indices.push_back(v);
			}
		}

		// Wrong number of indices
		if (indices.size() % 3 != 0)
			return false;

		// Rewrite indices to mesh builder
		for (MeshBuilder::Indice indice : indices)
			_mesh.addIndice(indice);

		return true;
	}
}