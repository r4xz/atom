#include "Atom/Object.h"

namespace Atom {
	const Matrix4<>& Object::transformation() const {
		return _local;
	}
}