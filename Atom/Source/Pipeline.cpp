#include "Atom/Pipeline.h"

namespace Atom {
	void Pipeline::render(std::shared_ptr<const Camera> camera, std::shared_ptr<const Object> scene) {
		// Prepare projection and view matrix
		Matrix4<> projection;
		if (camera->type == Camera::Perspective)
			projection = Matrix4<>::perspective(Angle::deg(90), 640.0f / 480.0f, 0.1f, 100.0f);
		else
			projection = Matrix4<>::orthographic(-1.0f, 1.0f, -1.0f, 1.0f, 0.1f, 100.0f);

		Matrix4<> view = camera->transformation();

		// Start render cycle
		scene->render(projection, view);
	}
}