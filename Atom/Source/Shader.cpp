#include "Atom/Shader.h"
#include <cassert>
#include <vector>
#include <cstdio>

namespace Atom {
	Shader::Shader() {
		_glProgram = glCreateProgram();
		assert(_glProgram != NULL);
	}
	Shader::~Shader() {
		glDeleteProgram(_glProgram);
	}

	void Shader::attach(const ShaderSource& source) {
		assert(source._glShader != NULL);

		glAttachShader(_glProgram, source._glShader);
	}

	bool Shader::link() {
		GLint param;

		// Link
		glLinkProgram(_glProgram);
		glGetProgramiv(_glProgram, GL_LINK_STATUS, &param);

		// Validate
		if (param == GL_TRUE) {
			glValidateProgram(_glProgram);
			glGetProgramiv(_glProgram, GL_VALIDATE_STATUS, &param);
		}

		// Check for errors
		if (param != GL_TRUE) {
			glGetProgramiv(_glProgram, GL_INFO_LOG_LENGTH, &param);
			std::vector<GLchar> log(param);

			glGetProgramInfoLog(_glProgram, param, NULL, &(log[0]));
			fprintf(stderr, "%s\n", &log[0]);

			return false;
		}

		// No errors found
		return true;
	}

	Shader::Location Shader::attribute(const GLchar* name) const {
		return glGetAttribLocation(_glProgram, name);
	}

	Shader::Location Shader::uniform(const GLchar* name) const {
		return glGetUniformLocation(_glProgram, name);
	}

	void Shader::use() const {
		glUseProgram(_glProgram);
	}
}