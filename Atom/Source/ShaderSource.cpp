#include "Atom/ShaderSource.h"
#include "Atom/FileReader.h"
#include <vector>

namespace Atom {
	ShaderSource::ShaderSource() : _glShader(NULL) {}

	ShaderSource::~ShaderSource() {
		dispose();
	}

	bool ShaderSource::compile(const char* path, Type type) {
		FileReader reader;
		if (!reader.open(path)) {
			fprintf(stderr, "Cannot open file '%s'.\n", path);
			return false;
		}

		// Create shader and copy source
		GLint length = (GLint) reader.length();
		const GLchar* content = reader.getAll();

		GLuint shader = glCreateShader(type);
		if (length <= 0 || shader == NULL) {
			fprintf(stderr, "Cannot read file or failed to create shader object.\n");
			return false;
		}

		glShaderSource(shader, 1, &content, &length);

		// Compile and validate
		GLint param;
		glCompileShader(shader);

		glGetShaderiv(shader, GL_COMPILE_STATUS, &param);
		if (param != GL_TRUE) {
			glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &param);
			std::vector<GLchar> log(param);

			glGetShaderInfoLog(shader, param, NULL, &(log[0]));
			fprintf(stderr, "%s\n", &log[0]);

			glDeleteShader(shader);
			return false;
		}

		// Apply changes
		dispose();
		_glShader = shader;

		return true;
	}

	void ShaderSource::dispose() {
		if (_glShader != NULL) {
			glDeleteShader(_glShader);
			_glShader = NULL;
		}
	}
}