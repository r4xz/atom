#include "Atom/ShaderUniform.h"

namespace Atom {
	void ShaderUniform::setColor(Shader::Location uniform, Color color) {
		// TODO: extend vector and use "color /= 255";
		glUniform4f(uniform, color.r / 255.0f, color.g / 255.0f, color.b / 255.0f, color.a / 255.0f);
	}

	void ShaderUniform::setValue(Shader::Location uniform, int val) {
		glUniform1i(uniform, val);
	}

	void ShaderUniform::setValue(Shader::Location uniform, uint val) {
		glUniform1ui(uniform, val);
	}

	void ShaderUniform::setValue(Shader::Location uniform, float val) {
		glUniform1f(uniform, val);
	}

	void ShaderUniform::setVector(Shader::Location uniform, const Vector2<>& vec) {
		glUniform2f(uniform, vec.x, vec.y);
	}

	void ShaderUniform::setVector(Shader::Location uniform, const Vector3<>& vec) {
		glUniform3f(uniform, vec.x, vec.y, vec.z);
	}

	void ShaderUniform::setVector(Shader::Location uniform, const Vector4<>& vec) {
		glUniform4f(uniform, vec.x, vec.y, vec.z, vec.w);
	}

	void ShaderUniform::setMatrix(Shader::Location uniform, const Matrix4<>& mat) {
		glUniformMatrix4fv(uniform, 1, GL_TRUE, mat.ptr());
	}
}