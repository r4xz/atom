#include "Atom/SolidMaterial.h"
#include "Atom/ShaderUniform.h"

namespace Atom {
	SolidMaterial::SolidMaterial(std::shared_ptr<const Shader> shader) : Material(shader) {
		initialize();
	}

	void SolidMaterial::use(const Matrix4<>& projection, const Matrix4<>& view, const Matrix4<>& model) const {
		_shader->use();

		ShaderUniform::setMatrix(_mvpUniform, projection * view * model);
		ShaderUniform::setColor(_colorUniform, color);
	}

	void SolidMaterial::initialize() {
		_shader->use();
		_vertexAttrib = _shader->attribute("vertex");
		_normalAttrib = Shader::InvalidLocation;
		_texCoordAttrib = Shader::InvalidLocation;

		_mvpUniform = _shader->uniform("mvp");
		_colorUniform = _shader->uniform("color");
	}
}