#include "Atom/TgaImage.h"
#include <cstring>

#define PX(x, y) _bitmap[x + y*_size.width]

namespace Atom {
	TgaImage::TgaImage() : _size(), _bitmap(NULL) {}

	TgaImage::~TgaImage() {
		dispose();
	}

	Color& TgaImage::operator()(uint x, uint y) {
		return PX(x, y);
	}

	Size<unsigned int> TgaImage::size() const {
		return _size;
	}
	
	void TgaImage::create(Size<uint> size) {
		dispose();

		_bitmap = new Color[size.area()];
		_size = size;
	}

	bool TgaImage::load(const char* path) {
		FileReader reader;
		if (!reader.open(path)) {
			fprintf(stderr, "Cannot open file %s.\n", path);
			return false;
		}

		// Image header
		Nullable<Header> header = readHeader(reader);
		if (!header.hasValue()) {
			return false;
		}

		// Skip identification field
		reader.setCursor(FileStream::Current, header->idLength);

		// Create bitmap
		create({ header->width, header->height });

		// Read pixels
		for (uint y = 0; y < _size.height; ++y) {
			for (uint x = 0; x < _size.width; ++x) {
				PX(x, y).b = reader.getChar();
				PX(x, y).g = reader.getChar();
				PX(x, y).r = reader.getChar();
				PX(x, y).a = reader.getChar();
			}
		}

		// Compare footer
		return isFooterCorrect(reader);
	}

	void TgaImage::repaint(Color color) {
		uint pixels = _size.area();
		for (uint i = 0; i < pixels; ++i) {
			_bitmap[i] = color;
		}
	}

	void TgaImage::dispose() {
		if (_bitmap != NULL) {
			delete[] _bitmap;
			_bitmap = NULL;
		}
	}

	Nullable<TgaImage::Header> TgaImage::readHeader(FileReader& reader) {
		assert(reader.isOpen());

		// fread/readBlock cannot be used due to compilers alignment
		// (turning off is not recommended for performance reasons).
		Header result;
		result.idLength = reader.getChar();
		result.colorMapType = reader.getChar();
		result.dataTypeCode = reader.getChar();
		result.colorMapOrigin = reader.getChar();
		result.colorMapOrigin += reader.getChar() << 8;
		result.colorMapLength = reader.getChar();
		result.colorMapLength += reader.getChar() << 8;
		result.colorMapDepth = reader.getChar();
		result.xOrigin = reader.getChar();
		result.xOrigin += reader.getChar() << 8;
		result.yOrigin = reader.getChar();
		result.yOrigin += reader.getChar() << 8;
		result.width = reader.getChar();
		result.width += reader.getChar() << 8;
		result.height = reader.getChar();
		result.height += reader.getChar() << 8;
		result.bitsPerPixel = reader.getChar();
		result.imageDescriptor = reader.getChar();
		
		// Check compatibility
		if (!isHeaderSupported(result)) {
			return Nullable<Header>();
		}

		return result;
	}

	bool TgaImage::isHeaderSupported(const Header& header) {
		int compatible = true;

		compatible &= header.dataTypeCode == 0x2; // Uncompressed RGB.
		compatible &= header.bitsPerPixel == 32; // RGBA format.
		compatible &= header.colorMapType == 0; // No color map included.
		compatible &= (header.imageDescriptor & 0x20) > 0; // Origin in upper left-hand corner.
		compatible &= (header.imageDescriptor & 0xC0) == 0; // Non-interleaved.

		if (!compatible) {
			fprintf(stderr, "At least one of the header flag is incompatible.\n");
			return false;
		}

		return true;
	}

	bool TgaImage::isFooterCorrect(FileReader& reader) {
		assert(reader.isOpen());

		reader.setCursor(FileStream::End, -18); // Last 18 bytes are constant.
		reader.readBlock(18);

		return strcmp(reader.buffer(), "TRUEVISION-XFILE.") == 0;
	}
}