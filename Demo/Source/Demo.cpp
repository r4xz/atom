#include <Atom/TgaImage.h>
#include <Atom/FileReader.h>
#include <Atom/MeshBuilder.h>
#include <Atom/ModelBuilder.h>
#include <Atom/SolidMaterial.h>
#include <Atom/Pipeline.h>
#include <GLFW/glfw3.h>
#include <memory>

using namespace Atom;

void run() {
	assert(glfwInit() == GL_TRUE);

	glfwWindowHint(GLFW_SAMPLES, 4);

	// Create window.
	GLFWwindow* window = glfwCreateWindow(640, 480, "Demo", NULL, NULL);
	assert(window != NULL);

	// Initialize OpenGL context.
	glfwMakeContextCurrent(window);
	assert(initContext());

	// Load assets.
	ShaderSource vertex;
	ShaderSource fragment;
	assert(vertex.compile("Resources/Shaders/solid.vs", ShaderSource::Vertex));
	assert(fragment.compile("Resources/Shaders/solid.fs", ShaderSource::Fragment));

	auto shader = std::make_shared<Shader>();
	shader->attach(vertex);
	shader->attach(fragment);
	assert(shader->link());

	/*MeshBuilder builder;
	builder.addVertex(Vector3<>(-1, -1, -5));
	builder.addVertex(Vector3<>(1, -1, -5));
	builder.addVertex(Vector3<>(0, 1, -5));
	builder.addIndice({ 1, 0, 0 });
	builder.addIndice({ 2, 0, 0 });
	builder.addIndice({ 3, 0, 0 });*/
	ModelBuilder builder;
	builder.load("Resources/Models/B-Wing.obj", shader);

	//auto solid = std::make_shared<SolidMaterial>(shader);
	//auto triangle = builder.build(solid);
	auto model = builder.build();

	Pipeline screen;
	auto camera = std::make_shared<Camera>();

	// Run demo.
	while (!glfwWindowShouldClose(window)) {
		glfwPollEvents();

		// TODO: move below settings to framebuffer/pipeline/material
		glEnable(GL_DEPTH);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
		glClearColor(0, 0, 0, 1);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//disasolid->color = Color(0, 255, 0, 255);
		screen.render(camera, model);

		glfwSwapBuffers(window);
	}

	glfwTerminate();
}

int main() {
	run();
	_CrtDumpMemoryLeaks();
	return 0;
}