#include "TestCore.h"
#include <Atom/Matrix4.h>
#include <glm/matrix.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace Tests {		
	TEST_CLASS(Matrix4Tests) {
	public:
		TEST_METHOD(rowMajorOrder) {
			Matrix4<int> mat(
				0, 1, 2, 3,
				4, 5, 6, 7,
				8, 9, 0, 1,
				2, 3, 4, 5
			);

			Assert::AreEqual(1, mat(1));
			Assert::AreEqual(9, mat(9));
			Assert::AreEqual(2, mat(12));
		}

		TEST_METHOD(defaultIdentity) {
			Matrix4<int> mat;

			Assert::AreEqual(1, mat(0, 0));
			Assert::AreEqual(1, mat(2, 2));
			Assert::AreEqual(0, mat(0, 1));
			Assert::AreEqual(0, mat(1, 0));
		}

		// @see: http://www.wolframalpha.com/input/?i=%7B%7B0%2C1%2C2%2C3%7D%2C%7B4%2C5%2C6%2C7%7D%2C%7B8%2C9%2C0%2C1%7D%2C%7B2%2C3%2C4%2C5%7D%7D%7B%7B6%2C2%2C2%2C3%7D%2C%7B0%2C3%2C9%2C7%7D%2C%7B4%2C5%2C4%2C1%7D%2C%7B4%2C3%2C1%2C5%7D%7D
		TEST_METHOD(matrixByMatrixMultiplication) {
			Matrix4<int> m(
				0, 1, 2, 3,
				4, 5, 6, 7,
				8, 9, 0, 1,
				2, 3, 4, 5
			);

			Matrix4<int> n(
				6, 2, 2, 3,
				0, 3, 9, 7,
				4, 5, 4, 1,
				4, 3, 1, 5
			);

			Matrix4<int> mat = m*n;

			Assert::AreEqual(20, mat(0, 0));
			Assert::AreEqual(84, mat(1, 2));
			Assert::AreEqual(52, mat(2, 0));
			Assert::AreEqual(56, mat(3, 3));
		}

		TEST_METHOD(lookAtTransformation) {
			Matrix4<> m = Matrix4<>::lookAt(
				Vector3<>(-7.3f, 3.0f, 4.0f),
				Vector3<>(5.0f, -3.2f, -8.5f),
				Vector3<>(3.0f, 1.4f, -2.9f)
			);

			glm::mat4 n = glm::lookAt(
				glm::vec3(-7.3f, 3.0f, 4.0f),
				glm::vec3(5.0f, -3.2f, -8.5f),
				glm::vec3(3.0f, 1.4f, -2.9f)
			);

			// glm use column major order:
			// [col][row] -> (row, col)
			for (int i = 0; i < 4; ++i) {
				for (int j = 0; j < 4; ++j) {
					Assert::AreEqual(n[i][j], m(j, i), 0.0001f);
				}
			}
		}

		TEST_METHOD(perspectiveProjection) {
			float fov = Angle::toRad(90.0f);
			float aspect = 1.78f; // ~1366/768
			float zNear = 1.0f;
			float zFar = 100.0f;

			Matrix4<> m = Matrix4<>::perspective(Angle::rad(fov), aspect, zNear, zFar);
			glm::mat4 n = glm::perspective(fov, aspect, zNear, zFar);

			// glm use column major order:
			// [col][row] -> (row, col)
			for (int i = 0; i < 4; ++i) {
				for (int j = 0; j < 4; ++j) {
					Assert::AreEqual(n[i][j], m(j, i), 0.0001f);
				}
			}
		}

		TEST_METHOD(orthographicProjection) {
			float left = -3.0f;
			float right = 5.3f;
			float top = 1.7f;
			float bottom = -2.3f;
			float zNear = 1.0f;
			float zFar = 100.0f;

			Matrix4<> m = Matrix4<>::orthographic(left, right, bottom, top, zNear, zFar);
			glm::mat4 n = glm::ortho(left, right, bottom, top, zNear, zFar);

			// glm use column major order:
			// [col][row] -> (row, col)
			for (int i = 0; i < 4; ++i) {
				for (int j = 0; j < 4; ++j) {
					Assert::AreEqual(n[i][j], m(j, i), 0.0001f);
				}
			}
		}
	};
}